package essensie.apimodels

import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec

case class AccessTokenResponse(val accessToken: String)

object AccessTokenResponse {
  implicit val accessTokenResponseCodec: Codec[AccessTokenResponse] =
    deriveCodec
}
