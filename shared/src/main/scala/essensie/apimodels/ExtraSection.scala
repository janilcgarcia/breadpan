package essensie.apimodels

import io.circe.Codec

case class ExtraSection(
  val name: String,
  val ingredients: Map[String, Double],
  val flourRatio: Double
) derives Codec.AsObject

