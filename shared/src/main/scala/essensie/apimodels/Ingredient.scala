package essensie.apimodels

import io.circe.Codec

case class Ingredient(
  val id: String,
  val name: String,
  val ratio: Double
) derives Codec.AsObject

object Ingredient {
  def apply(name: String, ratio: Double): Ingredient =
    Ingredient(Util.randomId(), name, ratio)
}
