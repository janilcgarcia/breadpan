package essensie.apimodels

import io.circe.Codec
import io.circe.syntax.*

case class LoginInput(val email: String, val password: String) derives Codec.AsObject
