package essensie.apimodels

import essensie.models.MealType

import java.time.OffsetDateTime
import java.time.ZoneId

import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec

case class MealInput(
  val id: Option[Long] = None,
  val description: String = "",
  val extraNotes: Option[String] = None,
  val hungerAtBeginning: Option[Int] = None,
  val hungerAtEnd: Option[Int] = None,
  val mealType: MealType = MealType.Other,
  val photos: Seq[Long] = Seq(),
  val ateAt: Option[OffsetDateTime] = Some(OffsetDateTime.now(ZoneId.of("UTC")))
)

object MealInput {
  implicit val mealInputCodec: Codec[MealInput] = deriveCodec
}
