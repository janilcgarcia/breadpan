package essensie.apimodels

import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec

case class MealPhotoResponse(val id: Long, val url: String)

object MealPhotoResponse {
  implicit val mealPhotoResponseCodec: Codec[MealPhotoResponse] = deriveCodec
}
