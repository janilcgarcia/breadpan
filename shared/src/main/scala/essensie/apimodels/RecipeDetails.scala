package essensie.apimodels

import io.circe.Codec

case class RecipeDetails(
  val id: Option[Long],
  val collectionId: Option[Long],
  val name: String,
  val baseFlourWeight: Double,
  val mainSection: MainSection,
  val extraSections: Seq[ExtraSection]
) derives Codec.AsObject
