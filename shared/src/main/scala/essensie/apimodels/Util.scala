package essensie.apimodels

import scala.util.Random

object Util {
  def randomId(size: Int = 10): String =
    Random.alphanumeric.take(size).mkString
}
