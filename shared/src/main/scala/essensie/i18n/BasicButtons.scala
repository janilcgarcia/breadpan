package essensie.i18n

case class BasicButtons(
  val add: String,
  val delete: String
)
