package essensie.i18n

import io.taig.babel.{Encoder, Decoder}
import io.taig.babel.generic.auto.deriveDecoder
import io.taig.babel.generic.auto.deriveEncoder

case class Labels(
  val recipePage: RecipePage,
  val basicButtons: BasicButtons,
  val loginPage: LoginPage
)

object Labels {
  given i18nDecoder: Decoder[Labels] = deriveDecoder[Labels]
  given i18nEncoder: Encoder[Labels] = deriveEncoder[Labels]
}
