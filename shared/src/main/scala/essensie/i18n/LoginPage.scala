package essensie.i18n

case class LoginPage(
  val email: String,
  val password: String,
  val invalidUser: String
)

