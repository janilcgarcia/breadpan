package essensie.models

import io.circe.Json
import io.circe.Encoder
import io.circe.Decoder
import io.circe.HCursor
import io.circe.DecodingFailure

sealed trait MealType
object MealType {
  case object Breakfast extends MealType
  case object Lunch extends MealType
  case object Tea extends MealType
  case object Dinner extends MealType
  case object Supper extends MealType
  case object Snack extends MealType
  case object Other extends MealType

  private val toStringValues: Map[MealType, String] = Map(
    Breakfast -> "breakfast",
    Lunch -> "lunch",
    Tea -> "tea",
    Dinner -> "dinner",
    Supper -> "supper",
    Snack -> "snack",
    Other -> "other"
  )

  private val fromStringValues: Map[String, MealType] = toStringValues.map {
    case (k, v) =>
      v -> k
  }

  val values: Seq[MealType] = Seq(
    Breakfast,
    Lunch,
    Tea,
    Dinner,
    Supper,
    Snack,
    Other
  )

  def stringEncode(t: MealType): String = toStringValues(t)

  def stringDecode(s: String): Option[MealType] = fromStringValues.get(s)

  implicit val mealTypeEncoder: Encoder[MealType] = Encoder
    .encodeString
    .contramap(stringEncode)

  implicit val mealTypeDecoder: Decoder[MealType] = Decoder
    .decodeString
    .flatMap { s =>
      stringDecode(s) match {
        case Some(t) =>
          Decoder.const(t)
        case None =>
          Decoder.failedWithMessage(s"Invalid mealType value $s")
      }
    }
}
