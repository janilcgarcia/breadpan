package essensie

import cats.syntax.applicativeError.*
import cats.syntax.monadError.*
import cats.syntax.applicative.*
import cats.syntax.flatMap.*
import cats.syntax.functor.*

import cats.effect.Sync
import org.http4s.HttpApp
import org.typelevel.log4cats.Logger
import org.http4s.Response
import org.http4s.Status
import org.http4s.Headers
import org.http4s.Entity
import org.typelevel.vault.Vault
import org.http4s.EntityEncoder
import org.http4s.InvalidBodyException
import org.http4s.InvalidMessageBodyFailure
import org.http4s.headers
import org.http4s.circe.CirceEntityCodec.*
import io.circe.*
import io.circe.syntax.*
import org.http4s.Media
import org.http4s.MediaType

object ErrorLoggingMiddleware {
  def apply[F[_]: Sync](app: HttpApp[F])(using
    L: Logger[F]
  ): HttpApp[F] = HttpApp { req =>
    app.apply(req).attempt.flatMap {
      case Left(exception: InvalidMessageBodyFailure) =>
        Response[F](
          status = Status.BadRequest,
          headers = Headers(
            headers.`Content-Type`(MediaType.application.json)
          ),
          entity = EntityEncoder[F, Json].toEntity(Map(
            "error" -> exception.getMessage
          ).asJson)
        ).pure

      case Left(exception) =>
        for {
          _ <- L.error(exception)(s"Error when processing $req")
        } yield Response[F](
          status = Status.InternalServerError,
          entity = EntityEncoder[F, String].toEntity("Ooops")
        )

      case Right(resp) =>
        resp.pure
    }
  }
}
