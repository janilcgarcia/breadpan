package essensie.config

import cats.syntax.*
import functor.*
import flatMap.*
import cats.effect.Sync

import pureconfig.{ConfigReader, ConfigSource}
import pureconfig.generic.derivation.default.*

import com.typesafe.config.ConfigFactory
import pureconfig.ConfigSource

/** Config object aggregating all other configurations.
  *
  * @param pgsql
  *   database config
  * @param server
  *   server config
  * @param security
  *   application security config
  */
case class AppConfig(
  val database: DatabaseConfig,
  val server: ServerConfig,
  val security: SecurityConfig
) derives ConfigReader

object AppConfig {
  def loadConfigOn[F[_]: Sync, A: ConfigReader](
    namespace: Option[String] = None
  ): F[A] = {
    val tsConfig = Sync[F].delay(
      ConfigFactory.load(getClass().getClassLoader())
    )

    for {
      config <- tsConfig.map(c => namespace.map(c.getConfig).getOrElse(c))

      result <- (
        ConfigSource.fromConfig(config).load[A] match {
          case Left(failures) =>
            Sync[F].raiseError(
              new ConfigLoadingException(failures.prettyPrint()))

          case Right(result) => Sync[F].pure(result)
        }
      )
    } yield result
  }

  def loadConfig[F[_]: Sync]: F[AppConfig] =
    loadConfigOn(None)
}
