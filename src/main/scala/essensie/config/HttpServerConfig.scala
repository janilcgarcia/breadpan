package essensie.config

import com.comcast.ip4s.{Host, Port}
import pureconfig.ConfigReader
import pureconfig.generic.derivation.default.*

/** Configuration for the server that serves HTTP.
  *
  * @param bind
  *   address to which bind the server, defaults to localhost.
  * @param port
  *   port of the server, defaults to 9000.
  */
case class HttpServerConfig(val bind: Host, val port: Port) derives ConfigReader
