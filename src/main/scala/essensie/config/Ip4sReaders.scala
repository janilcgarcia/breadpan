package essensie.config

import pureconfig.ConfigReader
import pureconfig.error.CannotConvert
import com.comcast.ip4s.{Host, Port}

given hostConfigReader: ConfigReader[Host] = ConfigReader[String].emap { s =>
  Host.fromString(s) match {
    case Some(host) =>
      Right(host)
    case None =>
      Left(CannotConvert(s, "Host", "doesn't look like a hostname"))
  }
}

given portConfigReader: ConfigReader[Port] = ConfigReader[Int].emap { port =>
  Port.fromInt(port) match {
    case Some(port) =>
      Right(port)
    case None =>
      Left(
        CannotConvert(port.toString, "Port", "is not in the valid port range")
      )
  }
}
