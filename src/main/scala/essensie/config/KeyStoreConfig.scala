package essensie.config

import java.io.File

import pureconfig.ConfigReader

/** KeyStore config for the HTTPS server.
  *
  * @param storeType
  *   Type of store, as defined in the Java spec.
  * @param path
  *   Path to the store.
  * @param password
  *   Password to open both the keystore and the secret key.
  */
case class KeyStoreConfig(
  val storeType: String,
  val path: File,
  val password: String
)

object KeyStoreConfig {
  given keyStoreConfigReader: ConfigReader[KeyStoreConfig] =
    ConfigReader.forProduct3("type", "path", "password")(KeyStoreConfig.apply)
}
