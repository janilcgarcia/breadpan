package essensie.config

import pureconfig.ConfigReader
import pureconfig.generic.derivation.default.*

/** Configuration related to the security of the APPLICATION. It does not cover
  * server security settings, like TLS and certificate settings.
  *
  * @param secretKey
  *   Key used in signing and encryption on the server. Should be big, random
  *   and secure. At least 128 bytes.
  */
case class SecurityConfig(val secretKey: String) derives ConfigReader
