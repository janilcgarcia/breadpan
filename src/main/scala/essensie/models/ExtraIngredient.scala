package essensie.models

case class ExtraIngredient(
  val id: Option[Long],
  val uniqueId: String,
  val flourWeight: Double,
  val index: Int,
  val sectionId: Long
)
