package essensie.models

import doobie.*
import doobie.implicits.*

object ExtraIngredientRepository {
  private val baseSelect =
    fr"""SELECT id, unique_id, flour_weight, position, section_id
    FROM extra_ingredients"""

  def getById(id: Long): ConnectionIO[ExtraIngredient] =
    sql"""$baseSelect WHERE id = $id"""
      .query[ExtraIngredient]
      .unique

  def getBySectionId(id: Long): ConnectionIO[List[ExtraIngredient]] =
    sql"""$baseSelect WHERE section_id = $id"""
      .query[ExtraIngredient]
      .to[List]

  def create(ingredient: ExtraIngredient): ConnectionIO[ExtraIngredient] =
    sql"""INSERT INTO extra_ingredients(unique_id, flour_weight, position, section_id)
    VALUES(${ingredient.uniqueId}, ${ingredient.flourWeight},
      ${ingredient.index}, ${ingredient.sectionId}
    )"""
      .update
      .withUniqueGeneratedKeys[ExtraIngredient](
        "id", "unique_id", "flour_weight", "position", "section_id"
      )

  def update(ingredient: ExtraIngredient): ConnectionIO[ExtraIngredient] =
    for {
      _ <- (
        sql"""UPDATE extra_ingredients SET
          unique_id = ${ingredient.uniqueId},
          flour_weight = ${ingredient.flourWeight},
          position = ${ingredient.index},
          section_id = ${ingredient.sectionId}
        WHERE id = ${ingredient.id}"""
      ).update.run

      ingredient <- getById(ingredient.id.get)
    } yield ingredient

  def delete(ingredient: ExtraIngredient): ConnectionIO[Int] =
    sql"""DELETE FROM extra_ingredients WHERE id = ${ingredient.id}"""
      .update.run
}
