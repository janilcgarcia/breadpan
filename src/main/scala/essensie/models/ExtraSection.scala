package essensie.models

case class ExtraSection(
  val id: Option[Long],
  val name: String,
  val relativeWeight: Double,
  val index: Int,
  val recipeId: Long
)
