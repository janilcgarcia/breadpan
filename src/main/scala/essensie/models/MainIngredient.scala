package essensie.models

case class MainIngredient(
  val id: Option[Long],
  val name: String,
  val flourWeight: Double,
  val uniqueId: String,
  val index: Int,
  val recipeId: Long
)
