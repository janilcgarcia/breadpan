package essensie.models

import doobie.*
import doobie.implicits.*
import cats.*
import cats.effect.*

object MainIngredientRepository {
  private val baseSelect =
    fr"""SELECT id, name, flour_weight, unique_id, position, recipe_id
      FROM main_ingredients"""

  def getById(id: Long): ConnectionIO[MainIngredient] =
    sql"""$baseSelect WHERE id = $id"""
      .query[MainIngredient]
      .unique

  def getForRecipe(recipeId: Long): ConnectionIO[List[MainIngredient]] =
    sql"""$baseSelect WHERE recipe_id = $recipeId ORDER BY position ASC"""
      .query[MainIngredient]
      .to[List]

  def create(ingredient: MainIngredient): ConnectionIO[MainIngredient] =
    sql"""INSERT INTO main_ingredients(name, flour_weight, unique_id,
      position, recipe_id
    ) VALUES (${ingredient.name}, ${ingredient.flourWeight}, ${ingredient.uniqueId},
      ${ingredient.index}, ${ingredient.recipeId}
    )"""
      .update
      .withUniqueGeneratedKeys[MainIngredient](
        "id", "name", "flour_weight", "unique_id", "position", "recipe_id")

  def delete(ingredient: MainIngredient): ConnectionIO[Int] =
    sql"""DELETE FROM main_ingredients WHERE id = ${ingredient.id}"""
      .update
      .run

  def update(ingredient: MainIngredient): ConnectionIO[MainIngredient] =
    for {
      _ <- (
        sql"""UPDATE main_ingredients SET
          name = ${ingredient.name},
          flour_weight = ${ingredient.flourWeight},
          unique_id = ${ingredient.uniqueId},
          position = ${ingredient.index},
          recipe_id = ${ingredient.recipeId}
        WHERE id = ${ingredient.id}"""
      ).update.run

      ingredient <- getById(ingredient.id.get)
    } yield ingredient

  def deleteForRecipe(recipeId: Long): ConnectionIO[Int] =
    sql"""DELETE FROM main_ingredients WHERE recipe_id = $recipeId"""
      .update
      .run
}
