package essensie.models

import essensie.apimodels.RecipeShort

case class Recipe(
  val id: Option[Long],
  val name: String,
  val flourWeight: Double,
  val userId: Long,
  val collectionId: Option[Long]
) {
  def asApiShort: RecipeShort = RecipeShort(
    id.get,
    name,
    collectionId
  )
}
