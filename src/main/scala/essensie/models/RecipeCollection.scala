package essensie.models

case class RecipeCollection(
  val id: Option[Long],
  val name: String,
  val userId: Long
)
