package essensie.models

import doobie.*
import doobie.implicits.*
import cats.*
import cats.effect.*
import cats.syntax.*

object RecipeCollectionRepository {
  private val baseSelect = fr"""SELECT id, name, user_id FROM recipe_collections"""

  def getByUser(id: Long): ConnectionIO[List[RecipeCollection]] =
    sql"""$baseSelect WHERE user_id = $id"""
      .query[RecipeCollection]
      .to[List]

  def getById(id: Long): ConnectionIO[RecipeCollection] =
    sql"""$baseSelect WHERE id = $id"""
      .query[RecipeCollection]
      .unique

  def create(collection: RecipeCollection): ConnectionIO[RecipeCollection] =
    sql"""INSERT INTO recipe_collections(name, user_id)
          VALUES (${collection.name}, ${collection.userId})"""
      .update
      .withUniqueGeneratedKeys[RecipeCollection]("id", "name", "user_id")

  def delete(collection: RecipeCollection): ConnectionIO[Int] =
    sql"""DELETE FROM recipe_collections WHERE id = ${collection.id.get}"""
      .update
      .run

  def update(collection: RecipeCollection): ConnectionIO[RecipeCollection] =
    for {
      _ <- (
        sql"""UPDATE recipe_collections SET
          name = ${collection.name}, user_id = ${collection.userId}
        WHERE id = ${collection.id.get}"""
          .update.run
      )

      user <- getById(collection.id.get)
    } yield user

}
