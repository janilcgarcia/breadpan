package essensie.models

import doobie.*
import doobie.implicits.*
import cats.*
import cats.effect.*
import cats.syntax.*

object RecipeRepository {
  private val baseSelect =
    fr"""SELECT id, name, flour_weight, user_id, collection_id FROM recipes"""

  def getById(id: Long): ConnectionIO[Recipe] =
    sql"""$baseSelect WHERE id = $id"""
      .query[Recipe]
      .unique

  def getByUserId(id: Long, collectionless: Boolean = false): ConnectionIO[List[Recipe]] = {
    val collFilter = if (collectionless)
      fr"AND collection_id is null"
    else
      fr""

    sql"""$baseSelect WHERE user_id = $id $collFilter"""
      .query[Recipe]
      .to[List]
  }

  def getByCollectionId(id: Option[Long]): ConnectionIO[List[Recipe]] =
    sql"""$baseSelect WHERE collection_id = $id"""
      .query[Recipe]
      .to[List]

  def create(recipe: Recipe): ConnectionIO[Recipe] =
    sql"""INSERT INTO recipes(name, flour_weight, user_id, collection_id)
    VALUES (${recipe.name}, ${recipe.flourWeight}, ${recipe.userId},
      ${recipe.collectionId})"""
      .update
      .withUniqueGeneratedKeys[Recipe](
        "id", "name", "flour_weight", "user_id", "collection_id")

  def delete(recipe: Recipe): ConnectionIO[Int] =
    sql"""DELETE FROM recipes WHERE id = ${recipe.id.get}"""
      .update
      .run

  def update(recipe: Recipe): ConnectionIO[Recipe] =
    for {
      _ <- (
        sql"""UPDATE recipes SET
          name = ${recipe.name},
          flour_weight = ${recipe.flourWeight},
          user_id = ${recipe.userId},
          collection_id = ${recipe.collectionId}
        WHERE id = ${recipe.id}"""
          .update.run
      )

      recipe <- getById(recipe.id.get)
    } yield recipe
}
