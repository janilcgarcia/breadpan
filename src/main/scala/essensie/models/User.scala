package essensie.models

import essensie.services.PasswordHasher
import java.time.OffsetDateTime

final case class User(
  val id: Option[Long],
  val email: String,
  val fullName: Option[String],
  val passwordHash: String,
  val addedAt: OffsetDateTime = OffsetDateTime.now()
) {
  def setPassword(hasher: PasswordHasher, password: String): User = copy(
    passwordHash = hasher.hashPassword(password)
  )

  def isPasswordCorrect(hasher: PasswordHasher, password: String): Boolean =
    hasher.isPasswordCorrect(passwordHash, password).valid
}

object User {
  def tupled = User.apply.tupled

  def make(
    email: String,
    fullName: Option[String],
    password: String,
    hasher: PasswordHasher
  ): User =
    User(
      None,
      email,
      fullName,
      hasher.hashPassword(password)
    )
}
