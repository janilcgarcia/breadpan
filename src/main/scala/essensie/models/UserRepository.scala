package essensie.models

import doobie.*
import doobie.implicits.*
import cats.*
import cats.effect.*
import cats.syntax.*
import functor.*
import applicative.*

import doobie.implicits.javatimedrivernative.*

import scala.concurrent.Future
import cats.effect.unsafe.IORuntime
import scala.concurrent.ExecutionContext

object UserRepository {
  private val baseSelect =
    fr"""SELECT id, email, full_name, password_hash, created_at
         FROM users"""

  def getById(id: Long): ConnectionIO[User] =
    sql"""$baseSelect WHERE id = $id"""
      .query[User]
      .unique

  def getByEmail(email: String): ConnectionIO[User] =
    sql"""$baseSelect WHERE email = $email"""
      .query[User]
      .unique

  def insert(user: User): ConnectionIO[User] =
    sql"""INSERT INTO users(email, full_name, password_hash)
          VALUES (${user.email}, ${user.fullName}, ${user.passwordHash})"""
      .update
      .withUniqueGeneratedKeys[User](
        "id", "email", "full_name", "password_hash", "created_at"
      )

  def updateUser(user: User): ConnectionIO[User] =
    for {
      _ <-
        sql"""UPDATE users SET 
          email = ${user.email},
          full_name = ${user.fullName},
          password_hash = ${user.passwordHash}
        WHERE id = ${user.id}""".update.run
      user <- getById(user.id.get)
    } yield user

  def deleteUser(user: User): ConnectionIO[Unit] =
    sql"""DELETE FROM users WHERE id = ${user.id}""".update.run.as(())
}
