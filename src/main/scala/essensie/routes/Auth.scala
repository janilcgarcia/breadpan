package essensie.routes

import java.sql.SQLException

import cats.data.{EitherT, Kleisli, OptionT}
import cats.{Monad, MonadError}

import cats.effect.{Concurrent, IO}

import cats.syntax.functor.*
import cats.syntax.applicative.*
import cats.syntax.semigroupal.*
import cats.syntax.applicativeError.*
import cats.syntax.monadError.*

import org.http4s.circe.CirceEntityCodec.*
import org.http4s.dsl.Http4sDsl
import org.http4s.dsl.io.*
import org.http4s.{
  ContextRequest,
  ContextRoutes,
  EntityDecoder,
  HttpRoutes
}

import doobie.Transactor
import doobie.implicits.*
import doobie.util.invariant

import io.circe.generic.semiauto.deriveCodec
import io.circe.syntax.*
import io.circe.{Codec, Json}

import essensie.models.{User, UserRepository}
import essensie.services.{PasswordHasher, SessionManager, Session}

import SessionManager.Extensions.*
import org.http4s.AuthedRoutes
import org.http4s.server.AuthMiddleware

import essensie.apimodels.{UserType, LoginInput}

class Auth[F[_]: Concurrent](
  xa: Transactor[F],
  hasher: PasswordHasher,
  sessionManager: SessionManager
) extends Http4sDsl[F] {
  private given SessionManager = sessionManager

  val loginRoutes: HttpRoutes[F] =
    HttpRoutes.of {
      case req @ POST -> Root / "login" =>
        (
          for {
            login <- OptionT.liftF(req.as[LoginInput])
            user <- (
              UserRepository
                .getByEmail(login.email)
                .transact(xa)
                .attemptT
                .toOption
            )
            _ <- OptionT.when(
              user.isPasswordCorrect(hasher, login.password)
            )(())
          } yield user
        ).foldF(
          Forbidden[Json](Json.obj(
            "error" -> "Invalid user or password!".asJson
          ))
        )(user =>
          Ok[Json](
            UserType(user.id.get, user.email, user.fullName).asJson
          ).map(_.setSession(Session(user.id)))
        )

      case POST -> Root / "logout" =>
        Ok[Json](Json.obj(
          "success" -> Json.True
        )).map(_.destroySession)
    }

  val queryRoutes: AuthedRoutes[User, F] = AuthedRoutes.of {
    case GET -> Root / "users" / "active" as user =>
      Ok(UserType(user.id.get, user.email, user.fullName).asJson)
  }

  val middleware: AuthMiddleware[F, User] = AuthMiddleware(Kleisli { req =>
    for {
      session <- OptionT.fromOption(req.getSession)
      userId <- OptionT.fromOption(session.userId)
      user <- (
        UserRepository
          .getById(userId)
          .transact(xa)
          .attemptT
          .toOption
      )
    } yield user
  })
}
