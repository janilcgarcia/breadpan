package essensie.routes

import cats.syntax.all.*
import cats.effect.*
import io.circe.syntax.*
import doobie.Transactor
import doobie.implicits.*
import org.http4s.AuthedRoutes
import org.http4s.circe.CirceEntityCodec.*
import org.http4s.dsl.Http4sDsl
import essensie.models.{User, RecipeCollectionRepository}
import essensie.apimodels
import essensie.apimodels.NewCollectionInput
import essensie.apimodels.SuccessfulDelete
import essensie.models.RecipeCollection

class CollectionRoutes[F[_]: Concurrent](
  xa: Transactor[F]
) extends Http4sDsl[F] {
  val create: AuthedRoutes[User, F] = AuthedRoutes.of {
    case req @ POST -> Root / "collections" / "create" as user =>
      for {
        input <- req.req.as[NewCollectionInput]

        collection <- (
          RecipeCollectionRepository
            .create(RecipeCollection(None, input.name, user.id.get))
            .transact(xa)
        )

        response <- Ok(apimodels.RecipeCollection(
          collection.id.get,
          collection.name,
          List()
        ))
      } yield response
  }

  val delete: AuthedRoutes[User, F] = AuthedRoutes.of {
    case req @ DELETE -> Root / "collections" / IntVar(id) as user =>
      for {
        collection <- (
          RecipeCollectionRepository
            .getById(id)
            .transact(xa)
        )

        response <- (
          if (collection.userId != user.id.get)
            Forbidden(Map(
              "error" -> "You're not authorized to perform this operation"
            ).asJson)
          else
            for {
              _ <- RecipeCollectionRepository.delete(collection).transact(xa)
              response <- Ok(SuccessfulDelete(collection.id.get).asJson)
            } yield response
        )
      } yield response
  }

  val routes = create <+> delete
}
