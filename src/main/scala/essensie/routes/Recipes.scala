package essensie.routes

import cats.Monad
import cats.effect.*
import cats.syntax.all.*
import io.circe.syntax.*
import doobie.{Transactor, ConnectionIO}
import doobie.implicits.*
import org.http4s.AuthedRoutes
import essensie.apimodels.RecipesResponse
import essensie.models.User
import org.http4s.dsl.Http4sDsl
import org.http4s.circe.CirceEntityCodec.*
import essensie.models.RecipeRepository
import essensie.models.RecipeCollectionRepository
import essensie.apimodels.RecipeShort
import essensie.apimodels.RecipeCollection
import essensie.apimodels.RecipeDetails
import cats.data.EitherT
import essensie.models.Recipe
import essensie.models.MainIngredientRepository
import essensie.models.MainIngredient
import essensie.apimodels.Ingredient
import essensie.models.ExtraSectionRepository
import essensie.models
import essensie.models.ExtraIngredientRepository
import essensie.apimodels.ExtraSection
import essensie.apimodels.MainSection
import cats.MonadError

class RecipesRoutes[F[_]: Concurrent](
  xa: Transactor[F]
) extends Http4sDsl[F] {
  val recipes: AuthedRoutes[User, F] = AuthedRoutes.of {
    case GET -> Root / "recipes" as user =>
      for {
        recipes <- (
          RecipeRepository
            .getByUserId(user.id.get, true)
            .transact(xa)
        )

        collections <- (
          RecipeCollectionRepository
            .getByUser(user.id.get)
            .flatMap { collections =>
              collections.map(coll =>
                RecipeRepository
                  .getByCollectionId(coll.id)
                  .map(_.map(_.asApiShort))
                  .map(recipes =>
                    RecipeCollection(
                      coll.id.get,
                      coll.name,
                      recipes
                    )
                  )
              )
              .sequence
            }
            .transact(xa)
        )

        resp <- Ok(RecipesResponse(
          recipes.map(_.asApiShort),
          collections
        ).asJson)
      } yield resp
  }

  val recipe: AuthedRoutes[User, F] = AuthedRoutes.of {
    case GET -> Root / "recipes" / LongVar(id) as user =>
      for {
        recipe <- (
          RecipeRepository
            .getById(id)
            .transact(xa)
        )

        ingredients <- (
          MainIngredientRepository
            .getForRecipe(id)
            .transact(xa)
            .map(_.map(ingredient =>
              Ingredient(
                ingredient.uniqueId, ingredient.name, ingredient.flourWeight)))
        )

        extraSections <- (
          ExtraSectionRepository
            .getByRecipeId(id)
            .flatMap(sections =>
              sections.map(section =>
                ExtraIngredientRepository
                  .getBySectionId(section.id.get)
                  .map(ingredients =>
                    ExtraSection(
                      section.name,
                      ingredients.map(i => i.uniqueId -> i.flourWeight).toMap,
                      section.relativeWeight
                    )
                  )
              ).sequence
            )
            .transact(xa)
        )

        resp <- (if (recipe.userId == user.id.get)
          Ok(RecipeDetails(
            recipe.id,
            recipe.collectionId,
            recipe.name,
            recipe.flourWeight,
            MainSection(
              "Main Section",
              ingredients
            ),

            extraSections
          ).asJson)
        else
          BadRequest(Map("message" -> "You can't access this recipe").asJson)
        )
      } yield resp
  }

  val saveRecipe: AuthedRoutes[User, F] = AuthedRoutes.of {
    case req @ POST -> Root / "recipes" as user =>
      for {
        recipe <- req.req.as[RecipeDetails]

        result <- updateOrCreateRecipe(recipe, user)

        resp <- (result match {
          case Left(error) => BadRequest(Map("message" -> error).asJson)
          case Right(result) => Ok(result.asJson)
        })
      } yield resp
  }

  private def updateOrCreateRecipe(
    recipe: RecipeDetails, user: User
  ): F[Either[String, RecipeDetails]] =
    (for {
      recipe <- (if (recipe.id.isEmpty)
        createRecipe(recipe, user)
      else
        updateRecipe(recipe, user)
      )
    } yield recipe).value

  private def clearRecipe(recipe: Recipe) =
    for {
      _ <- MainIngredientRepository.deleteForRecipe(recipe.id.get)
      _ <- ExtraSectionRepository.deleteForRecipe(recipe.id.get)
    } yield ()

  private def saveRecipeComponents(details: RecipeDetails, recipe: Recipe) = {
    val saveIngredients =
      details
        .mainSection
        .ingredients
        .zipWithIndex
        .map { (ingredient, index) =>
          MainIngredientRepository.create(
            MainIngredient(
              None,
              ingredient.name,
              ingredient.ratio,
              ingredient.id,
              index,
              recipe.id.get
            )
          ).map(ingredient =>
            Ingredient(ingredient.uniqueId, ingredient.name, ingredient.flourWeight)
          )
        }
        .sequence

    def saveSection(section: ExtraSection, index: Int) =
      for {
        dbSection <- ExtraSectionRepository.create(models.ExtraSection(
          None,
          section.name,
          section.flourRatio,
          index,
          recipe.id.get
        ))

        extraIngredients <- section.ingredients.zipWithIndex.map {
          case ((uniqueId, ratio), index) =>
            ExtraIngredientRepository.create(models.ExtraIngredient(
              None,
              uniqueId,
              ratio,
              index,
              dbSection.id.get
            )).map(ingredient =>
              ingredient.uniqueId -> ingredient.flourWeight
            )
        }.toSeq.sequence.map(_.toMap)
      } yield ExtraSection(
        dbSection.name,
        extraIngredients,
        dbSection.relativeWeight
      )

    val saveExtraSections =
      details
        .extraSections
        .zipWithIndex
        .map(saveSection.tupled)
        .sequence

    for {
      ingredients <- saveIngredients
      sections <- saveExtraSections
    } yield RecipeDetails(
      recipe.id,
      recipe.collectionId,
      recipe.name,
      recipe.flourWeight,
      MainSection("Main Section", ingredients),
      sections
    )
  }

  private def createRecipe(
    recipe: RecipeDetails, user: User
  ): EitherT[F, String, RecipeDetails] = {
    val createRecipeTx =
      for {
        r <- (
          RecipeRepository
            .create(
              Recipe(
                None,
                recipe.name,
                recipe.baseFlourWeight,
                user.id.get,
                recipe.collectionId
              )
            )
        )

        details <- saveRecipeComponents(recipe, r)
      } yield details

    createRecipeTx.transact(xa).attemptT.leftMap(_.getMessage)
  }

  private def updateRecipe(
    recipe: RecipeDetails, user: User
  ): EitherT[F, String, RecipeDetails] = {
    val updateRecipeTx =
      for {
        dbRecipe <- RecipeRepository.getById(recipe.id.get)

        _ <- Monad[ConnectionIO].whenA(dbRecipe.userId != user.id.get)(
          RuntimeException("You can't change this recipe").raiseError
        )

        _ <- clearRecipe(dbRecipe)
        details <- saveRecipeComponents(recipe, dbRecipe)
      } yield details

    updateRecipeTx.transact(xa).attemptT.leftMap(_.getMessage)
  }

  val routes = Seq(recipes, saveRecipe, recipe).reduce(_ <+> _)
}
