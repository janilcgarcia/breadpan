package essensie.services

import org.bouncycastle.crypto.generators.Argon2BytesGenerator
import org.bouncycastle.crypto.params.{Argon2Parameters as BCArgon2Parameters}

import java.util.Base64
import java.security.SecureRandom

import org.bouncycastle.util.Arrays

sealed trait PasswordResult {
  def valid: Boolean
}

object PasswordResult {
  sealed trait PasswordResultOk extends PasswordResult {
    def valid = true
  }

  sealed trait PasswordResultError extends PasswordResult {
    def valid = false
  }

  case object Ok extends PasswordResultOk
  case object NeedsReset extends PasswordResultOk
  case object UnknownAlgorithm extends PasswordResultError
  case object Invalid extends PasswordResultError
  case object BrokenHash extends PasswordResultError
}

trait PasswordHasher {
  def hashPassword(password: String): String
  def isPasswordCorrect(hashString: String, password: String): PasswordResult
}

object PasswordHasher {
  def apply(): PasswordHasher = MultipleAlgorithmPasswordHasher(
    ThreadLocal.withInitial(() => new SecureRandom())
  ).addAlgorithm(new Argon2("id", Argon2Parameters()))
}

private final case class MultipleAlgorithmPasswordHasher(
  secureRandom: ThreadLocal[SecureRandom],
  algorithms: Map[String, PasswordHashingAlgorithm] = Map(),
  defaultAlgorithm: Option[String] = Option.empty
) extends PasswordHasher {
  def addAlgorithm(
    algo: PasswordHashingAlgorithm
  ): MultipleAlgorithmPasswordHasher = copy(
    algorithms = algorithms + (algo.getName -> algo),
    defaultAlgorithm = defaultAlgorithm.orElse(Some(algo.getName))
  )

  def hashPassword(password: String): String = algorithms(
    defaultAlgorithm.getOrElse(algorithms.head._1)
  ).hashPassword(secureRandom.get(), password)

  def isPasswordCorrect(
    hashString: String,
    password: String
  ): PasswordResult = {
    val algo = hashString.substring(0, hashString.indexOf(':'))

    algorithms
      .get(algo)
      .map { algo =>
        algo.isPasswordCorrect(hashString, password)
      }
      .getOrElse(PasswordResult.UnknownAlgorithm)
  }
}

trait PasswordHashingAlgorithm {
  def getName: String
  def hashPassword(secureRandom: SecureRandom, password: String): String
  def isPasswordCorrect(hashString: String, password: String): PasswordResult
}

private final case class Argon2Parameters(
  val parallelism: Int = 2,
  val timeCost: Int = 7,
  val memoryCost: Int = 16
) {
  def asString: String = s"$parallelism:$timeCost:$memoryCost"
}

private class Argon2(val variant: String, val params: Argon2Parameters)
    extends PasswordHashingAlgorithm {
  require(Seq("i", "d", "id").contains(variant))

  def getName: String = s"argon2$variant"

  private def bcCode =
    variant match {
      case "i" =>
        BCArgon2Parameters.ARGON2_i
      case "d" =>
        BCArgon2Parameters.ARGON2_d
      case "id" =>
        BCArgon2Parameters.ARGON2_id
    }

  private def generateHash(
    params: Argon2Parameters,
    salt: Array[Byte],
    password: String
  ): Array[Byte] = {
    val parameters = new BCArgon2Parameters.Builder(bcCode)
      .withParallelism(params.parallelism)
      .withMemoryPowOfTwo(params.memoryCost)
      .withIterations(params.timeCost)
      .withSalt(salt)
      .build()

    val output = new Array[Byte](32)

    val generator = new Argon2BytesGenerator()
    generator.init(parameters)
    generator.generateBytes(password.getBytes("UTF-8"), output)

    output
  }

  def hashPassword(secureRandom: SecureRandom, password: String): String = {
    val saltBytes = new Array[Byte](16)
    secureRandom.nextBytes(saltBytes)

    val outBytes = generateHash(params, saltBytes, password)

    val b64Encoder = Base64.getEncoder.encodeToString

    val b64Salt = b64Encoder(saltBytes.toArray)
    val b64Out = b64Encoder(outBytes)

    s"$getName:${params.asString}:$b64Salt:$b64Out"
  }

  private case class OptionExtractor[A, R](val extractor: A => Option[R]) {
    def unapply(value: A): Option[R] = extractor(value)
  }

  private val maybeInt = OptionExtractor[String, Int](_.toIntOption)
  private val maybeBase64 = OptionExtractor[String, Array[Byte]](s =>
    Option(Base64.getDecoder().decode(s))
  )

  def isPasswordCorrect(
    hashString: String,
    password: String
  ): PasswordResult = {
    val algoName = getName
    hashString.split(':').toSeq match {
      case `algoName` +: maybeInt(parallelism) +: maybeInt(timeCost) +:
          maybeInt(memoryCost) +: maybeBase64(salt) +: maybeBase64(hash) +:
          Seq() =>
        val testHash = generateHash(
          Argon2Parameters(
            parallelism = parallelism,
            timeCost = timeCost,
            memoryCost = memoryCost
          ),
          salt,
          password
        )

        if (Arrays.constantTimeAreEqual(testHash, hash))
          if (params != this.params)
            PasswordResult.NeedsReset
          else
            PasswordResult.Ok
        else
          PasswordResult.Invalid
      case _ =>
        PasswordResult.BrokenHash
    }
  }
}
