package essensie.services

import io.circe.Codec
import io.circe.syntax.EncoderOps
import io.circe.generic.semiauto.deriveCodec
import io.circe.parser.decode
import essensie.services.CookieSigning

final case class Session(
  val userId: Option[Long]
) derives Codec.AsObject {

  def toCookie(sign: CookieSigning): String =
    sign.sign(this.asJson.noSpaces)
}

object Session {
  def fromCookie(sign: CookieSigning, contents: String): Option[Session] =
    for {
      verified <- sign.verify(contents)
      session <- decode[Session](verified).toOption
    } yield session
}
