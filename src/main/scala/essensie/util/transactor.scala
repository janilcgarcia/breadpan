package essensie.util

import cats.effect.Async
import doobie.util.transactor.Transactor
import essensie.config.DatabaseConfig

def makeTransactor[F[_]: Async](config: DatabaseConfig): Transactor[F] =
  Transactor.fromDriverManager[F](
    driver = config.driver,
    url = config.url,
    config.user,
    config.password
  )
