package essensie

import essensie.services.*
import java.security.SecureRandom
import org.bouncycastle.crypto.digests.Blake2bDigest

final class TestPasswordHashingAlgorithm extends PasswordHashingAlgorithm {
  def getName = "test"
  def hashPassword(secureRandom: SecureRandom, password: String): String =
    s"$getName:${password.hashCode()}"

  def isPasswordCorrect(hashString: String, password: String): PasswordResult = {
    val pw = hashString.substring(hashString.indexOf(':') + 1)
    if (pw.toInt == password.hashCode())
      PasswordResult.Ok
    else
      PasswordResult.Invalid
  }
}

final class TestPasswordHasher extends PasswordHasher {
  private val algo = TestPasswordHashingAlgorithm()

  def hashPassword(password: String) = algo.hashPassword(null, password)
  def isPasswordCorrect(hashString: String, password: String) =
    algo.isPasswordCorrect(hashString, password)
}
