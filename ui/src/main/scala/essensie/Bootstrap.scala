package essensie

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

@JSImport("bootstrap", JSImport.Namespace)
@js.native
object Bootstrap extends js.Object

@JSImport("bootstrap/dist/css/bootstrap.min.css", JSImport.Namespace)
@js.native
object BootstrapCSS extends js.Object
