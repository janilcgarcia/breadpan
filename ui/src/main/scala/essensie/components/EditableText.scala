package essensie.components

import com.raquo.laminar.api.L
import L.*


class EditableText(
  text: Seq[Modifier[Element]] => Element,
  initial: String = ""
) {
  private val internalValue = Var(initial)
  private val editting = Var(false)
  val value = internalValue.signal

  def set(value: String): Unit =
    internalValue.set(value)

  val root = editting.signal.splitOne(identity) { (isEditting, _, _) =>
    if (isEditting) {
      input(
        typ := "text",
        cls := ("form-control", "m-2"),

        controlled(
          L.value <-- internalValue,
          onInput.mapToValue --> internalValue
        ),

        onBlur.mapToStrict(false) --> editting,
        onKeyPress.filter(_.key == "Enter").mapToStrict(false) --> editting
      )
    } else {
      text(Seq(
        child.text <-- value,
        onDblClick.mapToStrict(true) --> editting
      ))
    }
  }
}

object EditableText {
  def h1(initial: String)(attrs: Modifier[Element]*): EditableText =
    EditableText(s => {
      val args = attrs ++ s
      L.h1(args*)
    }, initial)

  def h2(initial: String)(attrs: Modifier[Element]*): EditableText =
    EditableText(s => L.h2((attrs ++ s)*), initial)

  def h3(initial: String)(attrs: Modifier[Element]*): EditableText =
    EditableText(s => L.h3((attrs ++ s)*), initial)

  def h4(initial: String)(attrs: Modifier[Element]*): EditableText =
    EditableText(s => L.h4((attrs ++ s)*), initial)

  def h5(initial: String)(attrs: Modifier[Element]*): EditableText =
    EditableText(s => L.h5((attrs ++ s)*), initial)

  def h6(initial: String)(attrs: Modifier[Element]*): EditableText =
    EditableText(s => L.h6((attrs ++ s)*), initial)
}
