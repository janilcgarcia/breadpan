package essensie.components

import com.raquo.laminar.api.L
import L.*
import essensie.apimodels.Ingredient
import essensie.ui.TextInput
import essensie.i18n.Labels

class ExtraIngredientForm private (
  val labels: Labels,
  private val ingredients: Signal[Map[String, Ingredient]],
  private val unused: Signal[Set[String]],
  private val initial: Option[(String, Double)] = None
) extends Component[Div] {
  private val ratio = Var(initial.map(_._2).getOrElse(1.0))
  private val selectedId = Var(initial.map(_._1))

  private val deleteBus = new EventBus[AnyRef]

  private val available = ingredients.combineWithFn(selectedId.signal, unused) {
    (ingredients, selected, unused) =>

    (unused ++ selected.toSet).toSeq.map(ingredients.get).collect {
      case Some(ingredient) => ingredient
    }
  }

  val onDelete = deleteBus.events.mapToStrict(())

  val selected = selectedId.signal.combineWith(ratio.signal).map { (id, ratio) =>
    id.map(_ -> ratio)
  }

  private val nameSelect: Select = select(
    cls := "form-control",

    option(
      value := "",

      labels.recipePage.forms.chooseOne
    ),

    inContext { (self: Select) =>
      Seq(
        children <-- available.split(_.id)(makeOption(self)),

        self.events(onChange).mapTo(self.ref.value).map(v =>
          if (v.isEmpty)
            None
          else
            Some(v)
        ) --> selectedId.writer
      )
    },

    value <-- selectedId.signal.map(_.getOrElse(""))
  )

  private def makeOption(parent: => Select)(
    key: String, initial: Ingredient, signal: Signal[Ingredient]
  ) = option(
    value := key,

    child.text <-- signal.map(_.name)
  )

  val root = div(
    div(
      cls := ("row", "mb-3"),
      label(labels.recipePage.forms.ingredientName, cls := ("col-3", "col-form-label")),

      div(
        cls := "col-9",
        nameSelect
      )
    ),

    TextInput.Input(Some(labels.recipePage.forms.percentage))(
      "number",

      Seq(
        minAttr := "0.1",
        maxAttr := "100.0",
        stepAttr := "0.1",

        defaultValue <-- ratio.signal.map(r => (r * 100.0).toString),

        inContext { self =>
          self.events(onInput).debounce(100)
            .mapTo(self.ref.value.toDoubleOption).collect {
              case Some(ratio) => ratio / 100.0
              case None => 0.0
            } --> ratio.writer
        }
      )
    ),

    button(
      cls := ("btn", "btn-danger"),

      labels.basicButtons.delete,

      onClick --> deleteBus
    )
  )
}

object ExtraIngredientForm {
  def apply(
    i18n: Labels,
    available: Signal[Map[String, Ingredient]],
    unused: Signal[Set[String]],
    initial: Option[(String, Double)] = None
  ): ExtraIngredientForm = new ExtraIngredientForm(i18n, available, unused, initial)
}
