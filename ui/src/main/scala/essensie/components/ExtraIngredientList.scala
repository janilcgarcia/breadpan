package essensie.components

import com.raquo.laminar.api.L.*
import essensie.apimodels.Ingredient
import essensie.i18n.Labels

class ExtraIngredientList(
  val labels: Labels,
  private val onAdd: EventStream[Unit],
  private val ingredients: Signal[Map[String, Ingredient]],
  initial: Seq[(String, Double)],
  flour: Signal[Double],
  sectionRatio: Signal[Double]
) extends Component[Div] { self =>
  private val accordionId = Util.uniqueId("extra-ingredient-list")

  private class Item(
    val form: ExtraIngredientForm
  ) extends Component[Div] {
    val labels = self.labels

    val itemId = Util.uniqueId("extra-ingredient-list-item")
    val onDelete = form.onDelete
    val root = div(
      cls := "accordion-item",

      h2(
        cls := "accordion-header",
        idAttr := s"$itemId-header",

        button(
          cls := ("accordion-button", "collapsed"),
          typ := "button",
          dataAttr("bs-toggle") := "collapse",
          dataAttr("bs-target") := s"#$itemId",

          child.text <-- form.selected.signal
            .combineWithFn(ingredients, sectionRatio, flour) {
              (selected, ingredients, sectionRatio, flour) =>

              (for {
                (id, ratio) <- selected
                weight = sectionRatio * ratio * flour
                ingredient <- ingredients.get(id)
              } yield
                  f"${ingredient.name} - ${ratio * 100.0}%.1f %%"
              ).getOrElse("")
            }
        )
      ),

      div(
        idAttr := itemId,
        cls := ("accordion-collapse", "collapse", "p-3"),
        dataAttr("bs-parent") := s"#$accordionId",

        form.root
      )
    )
  }
  private val unused = Var(initial.map(_._1).toSet)

  private val items = Var(initial.map(initial =>
    Item(ExtraIngredientForm(labels, ingredients, unused.signal, Some(initial)))))

  private val used = items.signal.flatMap { items =>
    Signal.combineSeq(items.map { item =>
      item.form.selected
    }).map(_.collect {
      case Some(id -> _) => id
    }.toSet)
  }

  val usedIngredients = items.signal.flatMap { items =>
    Signal.combineSeq(items.map(_.form.selected)).map(_.collect {
      case Some(ingredient) => ingredient
    }.toMap)
  }

  private val onDeleteEvents = items.signal.flatMap { forms =>
    val deleteEvents = forms.zipWithIndex
      .map((form, index) => form.onDelete.mapToStrict(index))

    EventStream.mergeSeq(deleteEvents)
  }

  val root = div(
    cls := "accordion",
    idAttr := accordionId,

    children <-- items.signal.map(_.map(_.root)),

    onAdd --> items.updater { (seq, _) =>
      seq :+ Item(ExtraIngredientForm(labels, ingredients, unused.signal))
    },

    onDeleteEvents --> items.updater[Int] { (seq, index) =>
      seq.take(index) ++ seq.drop(index + 1)
    },

    ingredients.changes.withCurrentValueOf(items.signal.flatMap { items =>
      Signal.combineSeq(items.map(_.form.selected))
    }.map(_.zipWithIndex.collect {
      case Some(id -> _) -> index => id -> index
    })) --> items.updater[(Map[String, Ingredient], Seq[(String, Int)])] {
      (items, state) =>
      val (ingredients, ids) = state

      val excludedIndexes = ids.collect {
        case (id, index) if !ingredients.contains(id) => index
      }.toSet

      items.zipWithIndex.collect {
        case (item, index) if !excludedIndexes.contains(index) => item
      }
    },

    used.combineWithFn(ingredients.map(_.keySet))((used, all) => all -- used)
      --> unused
  )
}
