package essensie.components

import com.raquo.laminar.api.L
import L.*

import essensie.apimodels.ExtraSection
import essensie.apimodels.Ingredient
import essensie.ui.TextInput
import essensie.i18n.Labels

class ExtraSectionCard private (
  ingredients: Signal[Map[String, Ingredient]],
  flour: Signal[Double],
  initial: ExtraSection,
  val labels: Labels
) extends Component[Div] {
  private val name =
    EditableText(
      s => h4(s*).amend(cls := ("card-title", "mb-0")),
      initial.name
    )

  private val ratio = Var(initial.flourRatio)
  private val onItemAddBus = new EventBus[Unit]
  private val onDeleteBus = new EventBus[Unit]

  private val items = ExtraIngredientList(
    labels,
    onItemAddBus.events,
    ingredients,
    initial.ingredients.toSeq,
    flour,
    ratio.signal
  )

  val onDelete = onDeleteBus.events

  val section =
    name
      .value
      .combineWithFn(ratio.signal, items.usedIngredients) { (name, ratio, items) =>
        ExtraSection(name, items, ratio)
      }

  val root = div(
    cls := ("card", "mb-3"),
    div(
      cls := "card-body",

      div(
        cls := ("d-flex", "justify-content-between", "align-items-center", "mb-3"),

        child <-- name.root,

        div(
          button(
            cls := ("btn", "btn-primary"),
            labels.basicButtons.add,
            onClick.mapToStrict(()) --> onItemAddBus
          ),
          button(
            cls := ("btn", "btn-danger", "ms-2"),
            labels.basicButtons.delete,
            onClick.mapToStrict(()) --> onDeleteBus
          )
        )
      ),

      div(
        cls := "row",

        TextInput.Input(Some(labels.recipePage.baseFlourPercentage))(
          "number",
          Seq(
            controlled(
              value <-- ratio.signal.map(_ * 100.0).map(r => f"$r%.1f"),
              onInput.mapToValue --> ratio.writer
                .contramap[Double](_ / 100.0)
                .contramapOpt[String](_.toDoubleOption)
            ),

            minAttr := "0.1",
            maxAttr := "100.0",
            stepAttr := "0.1"
          )
        )
      ),
      items.root
    )
  )
}

object ExtraSectionCard {
  def apply(
    i18n: Labels,
  )(
    ingredients: Signal[Seq[Ingredient]],
    flour: Signal[Double],
    initial: ExtraSection = ExtraSection(
      i18n.recipePage.defaultExtraSectionTitle,
      Map(),
      1.0
    )
  ): ExtraSectionCard = new ExtraSectionCard(
    ingredients.map(_.map(item => item.id -> item).toMap),
    flour,
    initial,
    i18n
  )
}
