package essensie.components

import org.scalajs.dom

import com.raquo.laminar.api.L.*

import essensie.apimodels.Ingredient
import essensie.ui.TextInput
import com.raquo.laminar.nodes.ReactiveElement
import essensie.i18n.Labels

class IngredientForm(
  val root: Element,
  val ingredient: Signal[Ingredient],
  val onDelete: EventStream[Unit],
  val labels: Labels
) extends Component[Element] {
  def focusOnName(): Unit = {
    val nameInput = root.ref.getElementsByTagName("input")(0)
      .asInstanceOf[dom.html.Input]

    nameInput.focus()
  }
}

object IngredientForm {
  def apply(
    i18n: Labels,
    initial: Ingredient = Ingredient("", 1.0)
  ): IngredientForm = {
    val ingredientVar = Var(initial)
    val onDeleteBus = new EventBus[Unit]

    val nameSetter = ingredientVar.updater[String] { (ingredient, name) =>
      ingredient.copy(name = name)
    }

    val ratioSetter = ingredientVar.updater[Double] { (ingredient, ratio) =>
      ingredient.copy(ratio = ratio / 100.0)
    }.contramapOpt[String](_.toDoubleOption)

    val root = div(
      cls := "row",

      div(
        cls := "col",

        TextInput.Input(Some(i18n.recipePage.forms.ingredientName))(
          "text",
          Seq(
            controlled(
              value <-- ingredientVar.signal.map(_.name),
              onInput.mapToValue --> nameSetter
            )
          )
        ),

        TextInput.Input(Some(i18n.recipePage.forms.percentage))(
          "number",
          Seq(
            defaultValue <-- ingredientVar.signal
              .map(x => (x.ratio * 100.0).toString),

            maxAttr := "100.0",
            minAttr := "0.1",
            stepAttr := "0.1",

            onInput.mapToValue --> ratioSetter
          )
        ),

        button(
          cls := ("btn", "btn-danger"),
          i18n.basicButtons.delete,

          onClick.mapTo(()) --> onDeleteBus
        )
      )
    )

    new IngredientForm(root, ingredientVar.signal, onDeleteBus.events, i18n)
  }
}
