package essensie.components

import com.raquo.laminar.api.L
import L.*

import essensie.apimodels.Ingredient
import com.raquo.laminar.nodes.ReactiveElement
import org.scalajs.dom
import essensie.i18n.Labels

class IngredientList(
  val labels: Labels,
  private val onAdd: EventStream[Unit],
  initial: Seq[Ingredient],
  flour: Signal[Double]
) extends Component[Div] { self =>
  private val componentId = Util.uniqueId("ingredient-list")

  private class Item(
    val form: IngredientForm,
  ) extends Component[Div] {
    val labels = self.labels

    val itemId = Util.uniqueId("ingredient-list-item")

    val onDelete = form.onDelete

    val root = div(
      cls := "accordion-item",
      h2(
        cls := "accordion-header",
        idAttr := s"$itemId-header",

        button(
          cls := ("accordion-button", "collapsed"),
          typ := "button",
          dataAttr("bs-toggle") := "collapse",
          dataAttr("bs-target") := s"#$itemId",

          child.text <-- form.ingredient.combineWithFn(flour) { (ingredient, flour) =>
            val ratio = ingredient.ratio * 100.0
            val mass = ingredient.ratio * flour
            f"${ingredient.name} - ${ingredient.ratio * 100.0}%.1f %% - $mass g"
          }
        )
      ),

      div(
        idAttr := itemId,
        cls := ("accordion-collapse", "collapse", "p-3"),
        dataAttr("bs-parent") := s"#$componentId",

        form.root
      )
    )
  }

  private val items = Var(initial.map { ingredient =>
    Item(IngredientForm(labels, ingredient))
  })

  private def deleteUpater[A](items: Seq[A], index: Int) = {
    items.take(index) ++ items.drop(index + 1)
  }

  private val deleteEvents: EventStream[Int] =
    items.signal.flatMap(items =>
      EventStream.mergeSeq(
        items.zipWithIndex.map { (item, index) =>
          item.onDelete.mapTo(index)
        }
      )
    )

  def set(ingredients: Seq[Ingredient]): Unit =
    items.set(ingredients.map { ingredient =>
      Item(IngredientForm(labels, ingredient))
    })

  val values: Signal[Seq[Ingredient]] =
    items.signal.flatMap(items => Signal.combineSeq(items.map(_.form.ingredient)))

  val root = div(
    cls := "accordion",
    idAttr := componentId,

    children <-- items.signal.map(items => items.map(_.root)),

    onAdd --> items.updater { (items, _) =>
      items :+ Item(IngredientForm(labels))
    },

    deleteEvents --> items.updater(deleteUpater)
  )
}

object IngredientList {
  def apply(
    i18n: Labels,
    insertEvents: EventStream[Unit],
    initial: Seq[Ingredient],
    flour: Signal[Double]
  ): IngredientList =
    new IngredientList(
      i18n,
      insertEvents,
      initial,
      flour
    )
}
