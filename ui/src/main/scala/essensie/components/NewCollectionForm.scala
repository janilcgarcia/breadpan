package essensie.components

import com.raquo.laminar.api.L
import com.raquo.laminar.api.L.*
import essensie.i18n.Labels

class NewCollectionForm(val labels: Labels, val cancelable: Boolean)
    extends Component[FormElement] {
  import NewCollectionForm.*

  private val baseId = Util.uniqueId("new-collection-form")

  private val name = Var("")
  private val submitEvents = EventBus[Action]()

  val onSubmit = submitEvents.events.filter(_ == Submit).sample(name.signal)
  val onCancel = submitEvents.events.filter(_ == Cancel).mapToStrict(())

  val clear: Observer[Unit] = name.writer.contramap(_ => "")

  val root = form(
    cls := "form",

    h4("Create new Collection"),

    div(
      label(forId := s"$baseId-name", "Name"),
      input(
        typ := "text",
        cls := "form-control",
        idAttr := s"$baseId-name",

        controlled(
          value <-- name,
          onInput.mapToValue --> name
        )
      )
    ),

    div(
      cls := "mt-2",
      button(typ := "submit", cls := ("btn", "btn-primary"), "Save"),
      Option.when(cancelable)(
        button(
          typ := "button",
          cls := ("btn", "btn-danger", "ms-1"),
          "Cancel",
          onClick.mapToStrict(Cancel) --> submitEvents
        )
      )
    ),

    L.onSubmit.preventDefault.mapToStrict(Submit) --> submitEvents
  )
}

object NewCollectionForm {
  private sealed trait Action
  private case object Cancel extends Action
  private case object Submit extends Action
}
