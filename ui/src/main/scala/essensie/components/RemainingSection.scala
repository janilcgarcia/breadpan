package essensie.components

import com.raquo.laminar.api.L.*

import essensie.apimodels.Ingredient
import essensie.apimodels.ExtraSection
import essensie.i18n.Labels

class RemainingSection(
  val labels: Labels,
  ingredients: Signal[Seq[Ingredient]],
  extraSections: Signal[Seq[ExtraSection]],
  flour: Signal[Double]
) extends Component[Div] {
  private val remainingIngredients: Signal[Seq[(String, Double)]] =
    ingredients.combineWithFn(extraSections) { (ingredients, extraSections) =>
      ingredients.map { ingredient =>
        val ratio = ingredient.ratio - extraSections.map { section =>
          section.flourRatio * section.ingredients.getOrElse(ingredient.id, 0.0)
        }.sum

        ingredient.name -> ratio
      }
    }

  private val list = ul(
    cls := "list-group",

    children <-- extraSections.map(_.zipWithIndex).split(_._2) { (_, _, signal) =>
      li(
        cls := "list-group-item",

        child.text <-- signal.map { (section, _) =>
          labels.recipePage.allOf(section.name)
        }
      )
    },

    children <-- remainingIngredients.map(_.zipWithIndex).split(_._2) {
      (_, _, ingredient) =>
      li(
        cls := "list-group-item",
        child.text <-- ingredient.map(_._1).combineWithFn(flour) {
          case ((name, ratio), flour) =>
            f"$name - ${ratio * 100.0}%.1f %% - ${ratio * flour} g"
        }
      )
    }
  )

  val root = div(
    cls := ("card", "mt-3"),

    div(
      cls := "card-body",

      div(
        cls := ("d-flex", "justify-content-between", "align-items-center", "mb-3"),
        h4(labels.recipePage.remaining, cls := ("card-title", "mb-0"))
      ),

      list
    ),
  )
}
