package essensie.components

import scala.util.Random

object Util {
  private val charset = ('a' to 'z').toSeq ++ ('A' to 'Z').toSeq ++ ('0' to '9').toSeq

  def randomSample[A](seq: IndexedSeq[A]): LazyList[A] =
    seq(Random.nextInt(seq.size)) #:: randomSample(seq)

  def uniqueId(prefix: String): String = {
    val id = randomSample(charset).take(10).mkString
    s"$prefix-$id"
  }
}
