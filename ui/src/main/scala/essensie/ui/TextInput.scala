package essensie.ui

import com.raquo.laminar.api.L
import L._

object TextInput {
  def Input(labelContent: Option[String] = None)(
    inputType: String,
    inputModifiers: Seq[Modifier[Input]]
  ) = div(
    cls := ("row", "mb-3"),

    labelContent.map { content =>
      label(
        cls := ("col-3", "col-form-label"),
        content
      )
    },

    div(
      cls := "col-9",

      input(
        typ := inputType,
        cls := "form-control",

        inputModifiers
      )
    )
  )
}
