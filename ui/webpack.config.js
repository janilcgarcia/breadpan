const ScalaJS = require("./scalajs.webpack.config");
const { merge } = require("webpack-merge");

const WebApp = merge(ScalaJS, {
  mode: "development",
  
  // output: {
  //   hashFunction:"sha1" 
  // },
  
  module: {
    rules: [
      {
	test: /\.css$/,
	use: ["style-loader", "css-loader"]
      }
    ]
  }
});

module.exports = WebApp;
